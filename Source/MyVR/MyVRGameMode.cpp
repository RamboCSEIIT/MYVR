// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MyVRGameMode.h"
#include "MyVRHUD.h"
#include "MyVRCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyVRGameMode::AMyVRGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyVRHUD::StaticClass();
}
